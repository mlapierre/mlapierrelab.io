---
published: false
---
### To do

* Enable comments
  - Add google & facebook auth plugins
  - Other auth plugins?
  - Set up something to keep the heroku dyno alive so that the comments appear immediately

### Post ideas

* How to the principles of agile software development apply to testing?
http://agilemanifesto.org/principles.html
  * Simplicity--the art of maximizing the amount of work not done--is essential.
    - Scripted test cases typically violate this principle.

* A/B tests vs. RCTs

* Prescriptivism vs. descriptivism
  - or "it's ok to talk about automation vs. manual testing as long as you're not saying they should be separate"
  - or rules vs. heuristics
  - or dogma vs. karma (e.g., http://www.agitar.com/downloads/TheWayOfTestivus.pdf)

* Being a generalist
  - false dichotomy - almost everyone is a generalist with at least one specialisation
  - Anecdotal evidence that people are pro-specialist
    - https://huddle.eurostarsoftwaretesting.com/huddle-weekly-testers-poll-specialist-generalist-software-testing/
  - Specialist can't as effectively test interactions between components outside their area of expertise.
  - Awareness of potential issues outside area of expertise
    - Notice UX issues, refer to UX expert
    - Notice performance issues, refer to performance expert
    - Notice security issues, refer to security expert
    - Only vague understanding of those specialties means not noticing issues at all, or reaching the wrong conclusions about them, potentially even that there is no problem when there really is.

* Testing as experimentation
  - Call them experiments, not tests
  - Scripted testing is like following an instruction manual
    - You're given the precise steps and expected to not deviate
    - If anything goes wrong (missing pieces) you're lost
  - Exploratory testing is like performing an experiment
    - You have an outline of what to investigate, but no definite expectations
    - You might have guidelines/procedures to follow, but you're expected to adapt them to changing circumstances