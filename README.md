# Mark Lapierre's personal website

Powered by [Jekyll](http://jekyllrb.com/) & [Minimal Mistakes](https://mademistakes.com/work/minimal-mistakes-jekyll-theme/) & [GitLab Pages](https://about.gitlab.com/features/pages/)