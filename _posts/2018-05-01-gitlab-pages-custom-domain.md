---
layout: single
title:  "From GitHub to GitLab pages"
date:   2018-05-01 19:10:00 -0400
tags:   housekeeping
---
I started this site on [GitHub Pages](https://pages.github.com/) because:

  1. I was already using GitHub 
  2. I didn't want to have to deal with hosting
  3. It's free

But I'd been meaning to set up my own domain for a while<sup id="fnr1"><a href="#fn1">1</a></sup> and although GitHub pages supports custom domains it didn't seem to support HTTPS on custom domains. And there's no way I'm setting up a web site in 2018 without HTTPS.

It turns out that [support for HTTPS with custom domains is gradually being rolled out now](https://github.com/isaacs/github/issues/156#issuecomment-377262435), and it's possible to [set it up yourself](https://github.com/isaacs/github/issues/156#issuecomment-380159640). But there's no mention of this in the documentation, and [word from GitHub Support](https://github.com/isaacs/github/issues/156#issuecomment-366542067) doesn't suggest it'll be official any time soon. And it hasn't been rolled out to my account yet.

So I looked into GitLab as an alternative. I liked what I found. Not only is there [extensive documentation](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html), but there's also [an official tutorial](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/).

I won't say it was easy, but unless something has gone terribly wrong you're reading this over a secure connection certified by [Let's Encrypt](https://letsencrypt.org/).

I followed the instructions to [add my domain to my GitLab pages settings](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html#add-your-custom-domain-to-gitlab-pages-settings), and to [configure the DNS records for my domain](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html#dns-a-record). There are a few helpful links to how-to pages for specific hosts, but none for my registration service. Fortunately, the instruction provided were sufficient.

Before I delved into HTTPS I wanted to make sure the domain setup was behaving as expected. It was not. When I tried to open a link to my site I got a 404 back. The DNS record was directing the request to the correct server, but the server wasn't associating the request with my GitLab Pages repository. It was then that I realised the project name still ended in github.com, not gitlab.com. When I imported my GitHub repository, the import function on GitLab copied the original repo's name, [*ignoring the new name it allowed me to enter*](https://gitlab.com/gitlab-org/gitlab-ce/issues/43991). No matter, I thought, there's an option to change the name. So I did that. But still my site threw up a 404. It seems that I'd only changed the name of my *project*, i.e., the display name. I had to hunt down the setting to change the name of the repository itself (Settings > Advanced Settings > Rename repository). But once I'd updated that I could finally access my site via <a href="https://marklapierre.net" title="This site">marklapierre.net</a>.

And, finally, the HTTPS configuration. I followed [the tutorial](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/) and ran the `letsencrypt-auto` [CLI](https://en.wikipedia.org/wiki/Command-line_interface) to set up a challenge response that would confirm for Let's Encrypt that I control marklapierre.net. Unfortunately, at some point after the instructions were written [GitLab Pages and Jekyll stopped accepting permalinks with dots in them](https://gitlab.com/gitlab-org/gitlab-ce/issues/35853). Fortunately, [someone pointed this out in the comments on the tutorial](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/#comment-3685550418) along with the solution&mdash;end the permalink with `/` not `.html`.

So now I have a secure site on my own domain. Huzzah!

[_Update: on the same day I published this, [GitHub announced support for HTTPS on custom domains](https://blog.github.com/2018-05-01-github-pages-custom-domains-https/). Nice._]


<p style="font-size:12px" id="fn1"><a href="#fnr1">1.</a> Someone else got <a href="http://marklapierre.com" title="no relation">marklapierre.com</a>. Last update in 2014? C'mon Mark, you're killing me!